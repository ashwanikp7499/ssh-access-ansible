SSH-ACCESS
=========


Command To Grant Access
------------

    ansible-playbook -i inventory -e "action=grant" ssh.yml


Command Revoke Access
------------

    ansible-playbook -i inventory -e "action=revoke" ssh.yml

